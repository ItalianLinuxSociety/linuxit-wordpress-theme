<?php

// Register Custom Navigation Walker
require_once ('class-wp-bootstrap-navwalker.php');
// Add Customizer functionality
require_once get_template_directory() . '/inc/customizer.php';
// Hooks for various plugins
require_once get_template_directory() . '/inc/hooks.php';
// Multisite workarounds
require_once get_template_directory() . '/inc/multisite.php';

add_theme_support( 'wp-block-styles' );
add_theme_support( 'align-wide' );

function theme_setup () {
    // Featured Image Support
    add_theme_support('post-thumbnails');
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script' ) );
    add_theme_support( 'title-tag' );

    // Localization
    load_theme_textdomain( 'linuxit', get_template_directory() . '/languages' );

    // Nav Menus
    register_nav_menus (array (
        'primary' => __('Primary Menu', 'linuxit')
      ));

    // Post format support
    add_theme_support('post-formats', array('link'));
}

add_action('after_setup_theme', 'theme_setup');

function intit_widgets($id) {
  register_sidebar(array (
    'name' => 'Left Sidebar',
    'id' => 'left',
    'before_widget' => '<div class="row left-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
  ));
  register_sidebar(array (
    'name' => 'Right Sidebar',
    'id' => 'right',
    'before_widget' => '<div class="row right-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
  ));
}

add_action ('widgets_init', 'intit_widgets');

function linuxit_resource_hints( $hints, $relation_type ) {
    if ( 'preconnect' === $relation_type ) {
        $hints[] = '//www.linux.it';
    }

    return $hints;
}

add_filter( 'wp_resource_hints', 'linuxit_resource_hints', 10, 2 );

function add_theme_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap', 'https://www.linux.it/shared/?f=bootstrap.css', array(), 1.0);
    wp_enqueue_style( 'main', 'https://www.linux.it/shared/?f=main.css', array(), 1.0);
    wp_enqueue_script( 'bootstrap', 'https://www.linux.it/shared/index.php?f=bootstrap.js', array ( 'jquery' ), 1.0, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

?>
