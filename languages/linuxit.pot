#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: linuxit\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 3.0\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;"
"_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;"
"esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;"
"transChoice:1,2\n"
"X-Poedit-Basepath: ..\n"
"POT-Creation-Date: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: js\n"
"X-Poedit-SearchPathExcluded-1: node_modules\n"
"X-Poedit-SearchPathExcluded-2: src\n"

#: 404.php:14
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:17
msgid ""
"It looks like nothing was found at this location. Maybe try one of the "
"links below or a search?"
msgstr ""

#: class-wp-bootstrap-navwalker.php:369
msgid "Add a menu"
msgstr ""

#: comments.php:26
#, php-format
msgctxt "comments title"
msgid "One thought on &ldquo;%s&rdquo;"
msgstr ""

#: comments.php:32
#, php-format
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:48 comments.php:77
msgid "Comment navigation"
msgstr ""

#: comments.php:52 comments.php:81
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:58 comments.php:87
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:99
msgid "Comments are closed."
msgstr ""

#: comments.php:104
msgctxt "noun"
msgid "Comment"
msgstr ""

#: comments.php:106
msgid "Name"
msgstr ""

#: comments.php:108
msgid "Email"
msgstr ""

#: comments.php:110
msgid "Website"
msgstr ""

#: content/content-none.php:10
msgid "Nothing Found"
msgstr ""

#: content/content-none.php:17
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content/content-none.php:25
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: content/content-none.php:30
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: content/content-page.php:20
msgid "Pages:"
msgstr ""

#: content/content-page.php:29
msgid "Edit"
msgstr ""

#: content/content-single-archive.php:39 content/content-single.php:37
msgid ", "
msgstr ""

#: content/content-single-archive.php:41 content/content-single.php:39
msgid "Categories: "
msgstr ""

#: content/content-single.php:46
#, php-format
msgid "Edit %s"
msgstr ""

#: functions.php:22
msgid "Primary Menu"
msgstr ""

#: inc/customizer.php:5
msgid "Header"
msgstr ""

#: inc/customizer.php:6
msgid "Header settings"
msgstr ""
