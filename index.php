<?php
get_header();

if (have_posts()) {
	while (have_posts()) : the_post();
		if ( get_post_type() === 'page' ) {
            get_template_part( 'content/content', 'page' );
		} else {
            get_template_part( 'content/content', 'single-archive' );
        }
	endwhile;
} else {
	get_template_part( 'content/content', 'none' );
}

get_footer();
