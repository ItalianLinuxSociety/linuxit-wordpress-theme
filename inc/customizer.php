<?php

function linuxit_customize_register($wp_customize) {
  $wp_customize->add_section('header', array(
      'title'=> __('Header', 'linuxit'),
      'description'=>sprintf(__("Header settings", 'linuxit')),
      'priority'=>130,
    ));
  $wp_customize->add_setting('logo', array(
      'default'=>get_bloginfo('template_directory') . '/images/default-logo.png',
      'description'=>sprintf(__("Minimum size 90x90", 'linuxit')),
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'logo', array(
      'label'=> 'Logo',
      'section'=>'header',
      'setting'=>'logo',
      'priority', 1
    )));
  $wp_customize->add_setting('twitter', array(
	  'default'=>'https://twitter.com/ItaLinuxSociety',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('twitter', array(
      'label'=> 'Twitter',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('facebook', array(
	  'default'=>'https://www.facebook.com/ItaLinuxSociety/',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('facebook', array(
      'label'=> 'Facebook',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('instagram', array(
	  'default'=>'https://www.instagram.com/ItaLinuxSociety',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('instagram', array(
      'label'=> 'Instagram',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('telegram', array(
	  'default'=>'',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('telegram', array(
      'label'=> 'Telegram',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('youtube', array(
	  'default'=>'https://www.youtube.com/user/ItalianLinuxSociety',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('youtube', array(
      'label'=> 'Youtube',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('mastodon', array(
	  'default'=>'https://mastodon.uno/@ItaLinuxSociety/',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('mastodon', array(
      'label'=> 'Mastodon',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('github', array(
	  'default'=>'https://github.com/ItalianLinuxSociety',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('github', array(
      'label'=> 'GitHub',
      'section'=>'header',
      'priority', 2
  ));
  $wp_customize->add_setting('gitlab', array(
	  'default'=>'https://gitlab.com/ItalianLinuxSociety',
      'type'=>'theme_mod'
    ));
  $wp_customize->add_control('gitlab', array(
      'label'=> 'GitLab',
      'section'=>'header',
      'priority', 2
  ));
}

add_action('customize_register', 'linuxit_customize_register');
?>
