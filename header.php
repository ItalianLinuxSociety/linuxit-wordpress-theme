<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php wp_head(); ?>
</head>
<body id="page-top" <?php body_class(); ?>>
    <div id="header">
    <img src="<?php echo get_theme_mod('logo', get_bloginfo('template_directory') . '/images/default-logo.png'); ?>" alt="Logo" />
    <div id="maintitle"><?php bloginfo('name');?>
      <?php
        if (!empty(get_bloginfo('description'))) {
          ?>
          <div id="payoff"><?php bloginfo('description');?></div>
        <?php
        }
        ?>
    </div>

    <div class="menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <?php
              wp_nav_menu( array(
                  'theme_location'  => 'primary',
                  'depth'           => 0, // 1 = no dropdowns, 2 = with dropdowns.
                  'container'       => 'div',
                  'container_class' => 'collapse navbar-collapse',
                  'container_id'    => 'main-menu',
                  'menu_class'      => 'navbar-nav mr-auto',
                  'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                  'walker'          => new WP_Bootstrap_Navwalker(),
              ) );
            ?>
        </nav>

        <p class="social mt-2">
      <?php
      $socials = array('facebook','twitter','mastodon','instagram','gitlab','github','telegram','youtube');
      foreach ($socials as $social) {
        if (!empty(get_theme_mod($social))) {
          if ( $social === 'telegram' ) {
            $id = str_replace('https://t.me/joinchat/', '', get_theme_mod($social));
            echo "<script>
            jQuery(function() {
                jQuery('.telegram').each(function( index ) {
					var urlpartsplit = 'hat';
					var urlpart = 'joinc' + urlpartsplit;
					var urlpartagain = 't.';
					var urlpartanother = 'me/';
					var anotherid = '" . $id . "';
					jQuery(this).attr('href', 'https://' + urlpartagain + urlpartanother + urlpart + '/' + anotherid);
                });
            });
            </script>";
            echo '<a href="#" class="telegram"><img src="https://www.linux.it/shared/?f=immagini/' . $social . '.svg" alt="' . ucfirst($social) . '"></a>';
          } else {
            echo '<a href="' . get_theme_mod($social) . '"><img src="https://www.linux.it/shared/?f=immagini/' . $social . '.svg" alt="' . ucfirst($social) . '"></a>';
          }
        }
      }
      if ( defined( 'EM_VERSION' ) ) {
        echo '<a href="' . get_home_url() . '/events.ics"><img src="https://www.linux.it/shared/?f=immagini/calendar.svg" alt="Calendario Eventi"></a>';
      }
      ?>
        </p>
    </div>
  </div>
<div class="container mt-5 page-contents">
  <div class="row">
    <?php
      if (is_active_sidebar('left')) {
        echo "<div class='col-12 col-lg-3'>\n";
        dynamic_sidebar('left');
        echo "</div>\n";
      }
    ?>
    <div class="col">
