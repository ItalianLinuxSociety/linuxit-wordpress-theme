<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>

	<div class="entry-content">
        <?php
        the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'linuxit' ),
				'after'  => '</div>',
			)
		);
		?>

	</div>

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'linuxit' ), '<span class="edit-link">', '</span>' ); ?>
	</footer>
</article>
