<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<?php
		echo '<a href="' . get_the_permalink() . '">';
		the_title( '<h1 class="entry-title">', '</h1>' );
		echo '</a>';
		?>

		<div class="entry-meta">
			<?php
                $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
                $time_string = sprintf( $time_string,
                    esc_attr( get_the_date( 'c' ) ),
                    esc_html( get_the_date() ),
                    esc_attr( get_the_modified_date( 'c' ) ),
                    esc_html( get_the_modified_date() )
                );
                echo sprintf('<span class="posted-on">%1$s</span>', $time_string);
            ?>
		</div>
	</header>

	<div class="entry-content">
		<?php
			the_content();
		?>
	</div>

	<footer class="entry-footer">
		<?php
		if ( 'post' === get_post_type() ) {
			$categories_list = get_the_category_list( esc_html__( ', ', 'linuxit' ) );
			if ( $categories_list ) {
				_e('Categories: ','linuxit');
				printf( '<span class="cat-links">%s</span>', $categories_list );
			}
        }
        ?>
	</footer>
</article>
